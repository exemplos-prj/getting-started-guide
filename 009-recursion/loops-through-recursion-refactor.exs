defmodule Recursion do
  defp print(msg, add) do
    IO.puts msg <> " #{add} "
    add + 1
  end

  def print_multiple_times(msg, add, n) when n <= 1 do
    print msg, add
  end

  def print_multiple_times(msg, add, n) do
    add = print msg, add
    print_multiple_times(msg, add, n - 1)
  end
end

b = 47
x = Recursion.print_multiple_times("Hello!", b, 3)
IO.puts x + 2
