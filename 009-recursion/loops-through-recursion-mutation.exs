defmodule Recursion do
  defp print(%{m: m, v: v}) do
    IO.puts m <> " #{v} "
    %{m: m, v: v + 1}
  end

  def print_multiple_times(obj, n) when n <= 1 do
    print obj
  end

  def print_multiple_times(obj, n) do
    obj = print obj
    print_multiple_times(obj, n - 1)
  end
end

b = 47
x = Recursion.print_multiple_times(%{m: "Hello!", v: b}, 3)
IO.puts x.v + 2
