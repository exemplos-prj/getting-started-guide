odd? = &(rem(&1, 2) != 0)
total_sum = Enum.sum(Enum.filter(Enum.map(1..100_000, &(&1 * 3)), odd?))
IO.puts total_sum
