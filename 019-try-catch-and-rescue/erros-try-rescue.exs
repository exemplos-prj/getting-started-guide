try do
  raise "oops"
rescue
  e in RuntimeError -> e
end

try do
  raise "oops"
rescue
  RuntimeError -> "Error!"
end

case File.read "hello" do
  {:ok, body}      -> IO.puts "Success: #{body}"
  {:error, reason} -> IO.puts "Error: #{reason}"
end
